// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


const onlyAudiAndBmw = (inventories) => {
    if(Array.isArray(inventories)){
        let arr = [];
        for(let index = 0; index < inventories.length; index++){
            if(inventories[index].car_make === "BMW" || inventories[index].car_make === "Audi"){
                arr.push(inventories[index]);
            }
        }
        console.log(JSON.stringify(arr)); 
        return arr;
    } else{
        return `please enter the array.`
    }
}


module.exports = {onlyAudiAndBmw};