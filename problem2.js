/* The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
"Last car is a *car make goes here* *car model goes here*"*/


const lastCar = (inventories) => {
    if(Array.isArray(inventories)){
        let lastItem = inventories[(inventories.length) -1];
        return `Last car is a ${lastItem.car_make} ${lastItem.car_model}.`
    } else{
        return ` please enter the array.`
    }
}


module.exports = {lastCar};