// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.


const sortCarModel = (inventories) => {
    if(Array.isArray(inventories)){
        let arr = [];
        for(let index = 0; index < inventories.length; index++){
            arr.push(inventories[index].car_model);
        }
        return arr.sort()
    }
}

module.exports = {sortCarModel}

