// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.




const dealerData = (inventories) => {
    if(Array.isArray(inventories)){
        let arr = [];
        for(let index = 0; index < inventories.length; index++){
            arr.push(inventories[index].car_year);
        }
        return arr;
    } else{
        return `please enter the array.`
    }
}


module.exports = {dealerData};

