// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


const olderModels = (inventories,year = 2000) => {
    if(Array.isArray(inventories)){
        let arr = [];
        for(let index = 0; index < inventories.length; index++){
            if(inventories[index] < year){
                arr.push(inventories[index]);
            }
        }
        return arr;
    } else{
        return `please enter an array.`
    }
}

module.exports = {olderModels};