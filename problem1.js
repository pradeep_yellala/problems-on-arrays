/* The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"*/



function findCarById(inventories, id = 33){

    if(Array.isArray(inventories)){
        for(let index = 0; index < inventories.length; index++){
            if (inventories[index].id === id){
                return `Car ${id} is a ${inventories[index].car_year} ${inventories[index].car_make} ${inventories[index].car_model}.`;
            }
        }
    } else{
        return ` please give an array as input!`
    }
}


module.exports = {findCarById}